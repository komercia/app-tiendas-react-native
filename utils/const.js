import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Badge } from "native-base";

const styles = StyleSheet.create({
  buttonLogin: {
    width: 300,
    backgroundColor: "#f14b5a",
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 8
  },
  alignButton: {
    flexDirection: "row",
    alignItems: "center"
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  }
});

export const Bold = props => (
  <Text style={{ fontWeight: "bold" }}>{props.children}</Text>
);

// #4429AE
// #4a59f5

export const Colors = {
  first: "#4429AE",
  // first: "#4a59f5",
  second: "#00DD8D",
  // second: "#30c490",
  third: "#f5aa3a",
  fourth: "#db5d2a",
  red: "#ff585a",
  another: "#12cc6f",
  morado_oscuro: "#1c2260",
  morado_claro: "#5c6af6",
  morado_claro_usado_en_alertas: "#4a59f5",
  verde_nuevo: "#57ad2a",
  verde_aguamarina_oscuro: "#0F9380",
  verde_aguamarina_claro: "#00c894",
  rojito_medio_morado: "#f14b5a",
  badge: "#0000ff",
  input_background: "#F6F7FA",
  placeholder_input: "#B0C4DE"
};

export const Header = () => {
  <Header style={{ backgroundColor: "#2f3542" }}>
    <Left>
      <Button transparent onPress={() => navigate("Dash")}>
        <Icon name="arrow-left" color="white" size={20} />
      </Button>
    </Left>
    <Body>
      <Title>Inventario</Title>
    </Body>
    <Right>
      <TouchableOpacity
        style={styles.cancelButton}
        onPress={() => navigate("NewSaleBarcode")}
      />
    </Right>
  </Header>;
};

// 0. Sin pagar
// 1.  Pagada
// 2.  Rechazada (Vendedor)
// 3.  Cancelado (Comprador)
// 4.  Despachado
// 5.  Recibido por el cliente
// 6.  Finalizado y entregado
// 7.  Finalizado (Comprador)
// 8. Cotizacion
// 9. Procesando pago
// 10. Rechazado (Pasarela)

export const SalesState = props => {
  let status = "";
  let color = "white";
  let sale = props;
  switch (sale) {
    case "0":
      status = "Sin pagar";
      color = "#ffa801";
      break;
    case "1":
      status = "Pagada";
      color = "#30c490";
      break;
    case "2":
      status = "Rechazada";
      color = "#eb4d4b";
      break;
    case "3":
      status = "Cancelado";
      color = "#eb4d4b";
      break;
    case "4":
      status = "Despachado";
      color = "#0000ff";
      break;
    case "5":
      status = "Recibido";
      color = "#30336b";
      break;
    case "6":
      status = "Entregado";
      color = "#4429AE";
      break;
    case "7":
      status = "Finalizado";
      color = "#0000ff";
      break;
    case "8":
      status = "Cotización";
      color = "#706fd3";
      break;
    case "9":
      status = "Procesando";
      color = "#ffa801";
      break;
    case "10":
      status = "Rechazada";
      color = "#eb4d4b";
      break;
  }
  return (
    <View>
      <Badge style={{ backgroundColor: color }}>
        <Text style={{ color: "white", padding: 3 }}>{status}</Text>
      </Badge>
    </View>
  );
};

export const formatCurrency = n => {
  if (n) {
    return parseInt(n)
      .toFixed()
      .replace(/(\d)(?=(\d{3})+(,|$))/g, "$1,");
  } else {
    return 0;
  }
};

// METODOS PAGO
// 1 - payco tc
// 2 - payco pse
// 3 - payco efectivo
// 4 - consignacion
// 5 - efecty
// 6 - pago en tienda
// 7 - convenir
// 8 - payco SafetyPay(Banca internacional)
// 9 - contraentrega
// 10 - mercadopago TC
// 11 - payu tc
// 12 - payu efectivo
// 13 - payu pse
// 14 - Mercadopago PSE

export const Payments = props => {
  let status = "Sin especificar";
  let color = "white";
  let sale = props;
  switch (sale) {
    case "1":
      status = "Tarjeta de credito";
      color = "#ffa801";
      imagen = require("../src/assets/payments/mercadopago.png");

      break;
    case "2":
      status = "ePayco PSE";
      color = "#30c490";
      imagen = require("../src/assets/payments/mercadopago.png");

      break;
    case "3":
      status = "ePayco efectivo";
      color = "#eb4d4b";
      imagen = require("../src/assets/payments/mercadopago.png");

      break;
    case "4":
      status = "Consignación";
      color = "#eb4d4b";
      imagen = require("../src/assets/payments/store.png");

      break;
    case "5":
      status = "Pago en Efecty";
      color = "#4834d4";
      imagen = require("../src/assets/payments/efecty.png");

      break;
    case "6":
      status = "Paga en Tienda";
      color = "#30336b";
      imagen = require("../src/assets/payments/store.png");

      break;
    case "7":
      status = "Pago a convenir";
      color = "#6ab04c";
      imagen = require("../src/assets/payments/convenir.png");

      break;
    case "8":
      status = "ePayco - SafetyPay";
      color = "#0000ff";
      imagen = require("../src/assets/payments/mercadopago.png");

      break;
    case "9":
      status = "Pago contraentrega";
      color = "#706fd3";
      imagen = require("../src/assets/payments/store.png");

      break;
    case "10":
      status = "Tarjeta de crédito";
      color = "#ffa801";
      imagen = require("../src/assets/payments/mercadopago.png");

      break;
    case "11":
      status = "Tarjeta de crédito";
      color = "#eb4d4b";
      imagen = require("../src/assets/payments/mercadopago.png");

      break;
    case "12":
      status = "Payu - efectivo";
      color = "#eb4d4b";
      imagen = require("../src/assets/payments/mercadopago.png");

      break;
    case "13":
      status = "Payu PSE";
      color = "#eb4d4b";
      imagen = require("../src/assets/payments/mercadopago.png");

      break;
    case "14":
      status = "MercadoPago - PSE";
      color = "#eb4d4b";
      imagen = require("../src/assets/payments/mercadopago.png");
      break;
  }
  return (
    <View
      style={{
        width: 140,
        alignItems: "flex-start",
        justifyContent: "center",
        marginTop: 15
      }}
    >
      <View
        style={{
          width: 140,
          borderRadius: 4,
          justifyContent: "center",
          alignItems: "center",
          padding: 2,
          elevation: 1,
          paddingHorizontal: 16,
          backgroundColor: "#FFFFFF"
        }}
      >
        <Image
          resizeMode="contain"
          style={{ width: 120, height: 50 }}
          source={imagen}
        />
      </View>
      <Text
        style={{
          color: "black",
          marginTop: 5,
          marginBottom: 5,
          fontSize: 12,
          alignSelf: "center"
        }}
      >
        {status}
      </Text>
    </View>
  );
};

// ESTADOS VENTAS PLANES KOMERCIA (EPAYCO)
// 1    Aceptada
// 2    Rechazada
// 3    Pendiente
// 4    Fallida
// 6    Reversada
// 7    Retenida
// 8    Iniciada
// 9    Exprirada
// 10   Abandonada
// 11   Cancelada
// 12   Antifraude

// ESTADOS COMPRA
// 0. Sin pagar
// 1.  Pagada
// 2.  Rechazada (Vendedor)
// 3.  Cancelado (Comprador)
// 4.  Despachado
// 5.  Recibido
// 6.  Finalizado y entregado
// 7.  Finalizado (Comprador)
// 8. Cotizacion
// 9. Procesando pago
// 10. Rechazado (Pasarela)
