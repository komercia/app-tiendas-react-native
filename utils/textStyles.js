export const TextStyle = {
  title: { fontWeight: "600", fontSize: 25 },
  textLink: {
    fontSize: 12,
    color: "#484848",
    fontWeight: "600"
  }
};
