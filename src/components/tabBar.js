import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { Badge } from "native-base";
import Icon from "react-native-vector-icons/Feather";
import { Colors } from "../../utils/const";
import { connect } from "react-redux";

class NavBar extends React.Component {
  render() {
    const position = this.props.position;
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.tabBar}>
        {position == 1 ? (
          <View style={styles.tabItemSelected}>
            <View style={styles.cicrleSelected}>
              <Image source={require("../../src/assets/menu/home.png")} />
              <Text style={styles.subtextActive}>Inicio</Text>
            </View>
          </View>
        ) : (
          <TouchableOpacity
            style={styles.tabItem}
            onPress={() => navigate("Dash")}
          >
            <Image source={require("../../src/assets/menu/home2.png")} />
            <Text style={styles.subtext}>Inicio</Text>
          </TouchableOpacity>
        )}

        {position == 2 ? (
          <View style={styles.tabItemSelected}>
            <View style={styles.cicrleSelected}>
              <Image source={require("../../src/assets/menu/stats.png")} />
              <Text style={styles.subtextActive}>Inventario</Text>
            </View>
          </View>
        ) : (
          <TouchableOpacity
            style={styles.tabItem}
            onPress={() => navigate("ListProducts")}
          >
            <Image source={require("../../src/assets/menu/stats2.png")} />
            <Text style={styles.subtext}>Inventario</Text>
          </TouchableOpacity>
        )}

        <View style={styles.contentTabCart}>
          <TouchableOpacity
            style={styles.tabCart}
            onPress={() => navigate("NewSaleGeneralDetails")}
          >
            <Icon name="shopping-cart" style={styles.tabCartIcon} />

            {this.props.cartItems.length > 0 ? (
              <Badge
                style={{
                  width: 21,
                  height: 21,
                  position: "absolute",
                  backgroundColor: "#ff585a",
                  marginLeft: 35,
                  marginTop: 50
                }}
              >
                <Text style={{ color: "#fff" }}>
                  {this.props.cartItems.length}
                </Text>
              </Badge>
            ) : (
              <View />
            )}
          </TouchableOpacity>
        </View>

        {position == 3 ? (
          <View style={styles.tabItemSelected}>
            <View style={styles.cicrleSelected}>
              <Image source={require("../../src/assets/menu/history.png")} />
              <Text style={styles.subtextActive}>Ventas</Text>
            </View>
          </View>
        ) : (
          <TouchableOpacity
            style={styles.tabItem}
            onPress={() => navigate("Sales")}
          >
            <Image source={require("../../src/assets/menu/history2.png")} />
            <Text style={styles.subtext}>Ventas</Text>
          </TouchableOpacity>
        )}

        {position == 4 ? (
          <View style={styles.tabItemSelected}>
            <View style={styles.cicrleSelected}>
              <Image source={require("../../src/assets/menu/account.png")} />
              <Text style={styles.subtextActive}>Tienda</Text>
            </View>
          </View>
        ) : (
          <TouchableOpacity
            style={styles.tabItem}
            onPress={() => navigate("Settings")}
          >
            <Image source={require("../../src/assets/menu/account2.png")} />
            <Text style={styles.subtext}>Tienda</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    cartItems: state.cartItems
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: () => dispatch({ type: "ADD_TO_CART", payload: "hola" })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavBar);

const styles = StyleSheet.create({
  tabBar: {
    height: 55,
    backgroundColor: "#ffffff",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    alignContent: "center",
    borderColor: "#ffffff",
    borderTopWidth: 0.5,
    paddingHorizontal: 10,
    elevation: 12
  },
  tabItem: {
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
    flex: 1,
    height: "100%"
  },
  cicrleSelected: {
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center"
    // backgroundColor: "#F1F1F2",
    // padding: 5,
    // borderRadius: 40,
    // width: 60,
    // height: 60,
    // alignItems: "center",
    // justifyContent: "center",
    // elevation: 1
  },
  tabItemSelected: {
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
    flex: 1,
    height: "100%",
    // borderRadius: 20,
    // height: 40,
    // width: 20
    marginTop: -1
    //  elevation: 1
  },
  tabIcon: {
    color: "#90a4ae",
    fontSize: 22,
    fontWeight: "100"
  },
  tabTitle: {
    marginTop: 3,
    fontSize: 13,
    color: "#90a4ae"
  },
  tabIconSelected: {
    color: Colors.first,
    fontSize: 21,
    fontWeight: "900"
  },
  tabTitleSelected: {
    marginTop: 3,
    fontSize: 12,
    color: Colors.first,
    fontWeight: "600"
  },
  contentTabCart: {
    zIndex: 9999,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 55,
    width: 55,
    marginTop: -25
  },
  tabCart: {
    zIndex: 9999,
    backgroundColor: Colors.second,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 46,
    width: 46,
    elevation: 5
  },
  tabCamera: {
    zIndex: 9999,
    backgroundColor: Colors.verde_aguamarina_claro,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 55,
    width: 55,
    elevation: 6,
    marginTop: -35
  },
  tabCartIcon: {
    color: "#4429AE",
    fontSize: 20,
    fontWeight: "100"
  },
  tabCartTitle: {
    marginTop: 3,
    fontSize: 10,
    color: "#90a4ae"
  },
  subtext: {
    fontSize: 11,
    color: "#90a4ae",
    marginTop: 3
  },
  subtextActive: {
    fontSize: 11,
    color: Colors.first,
    marginTop: 3
  }
});
