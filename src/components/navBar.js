import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TouchableHighlight
} from "react-native";
import { Colors } from "../../utils/const";
import Icon from "react-native-vector-icons/Feather";
import FastImage from "react-native-fast-image";

const NavBar = ({ navigation, user, store }) => {
  return (
    <SafeAreaView style={styles.navBar}>
      <View style={styles.profileContent}>
        <TouchableOpacity onPress={() => navigation.navigate("Settings")}>
          {user.id_facebook ? (
            <FastImage
              onPress={() => navigation.navigate("Settings")}
              style={styles.imageProfile}
              source={{
                uri: `https://graph.facebook.com/${user.id_facebook}/picture?type=large&width=520&height=520`
              }}
            />
          ) : (
            <Image
              onPress={() => navigation.navigate("Settings")}
              style={styles.imageProfile}
              source={{ uri: `https://api2.komercia.co/users/${user.foto}` }}
            />
          )}
        </TouchableOpacity>
        <View>
          {/* <Text style={styles.storeName}>{store.nombre}</Text> */}
          <Text style={styles.storeName}>Hola {user.nombre}</Text>
          <Text style={styles.userName}>Administrador</Text>
        </View>
      </View>

      <TouchableOpacity
        onPress={() => navigation.navigate("Settings")}
        style={{
          alignItems: "center",
          justifyContent: "center",
          alignContent: "center",
          height: 55
        }}
      >
        {/* <Icon
          name="settings"
          size={20}
          color={"white"}
          style={styles.iconSettings}
        /> */}
        {/* <Image
          style={{ marginRight: 21, width: 26, height: 26 }}
          source={require("../../src/assets/notification.png")}
        /> */}
        <Image
          source={require("../logos/iso.png")}
          style={{ height: 29, width: 29, marginRight: 21 }}
        />
      </TouchableOpacity>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  navBar: {
    height: 20,
    flexDirection: "row",
    alignContent: "center",
    alignItems: "center",
    marginBottom: 15,
    width: "100%",
    justifyContent: "space-between",
    marginBottom: 100,
    marginTop: 35
  },
  profileContent: {
    flexDirection: "row",

    marginLeft: 15,
    alignItems: "center",
    width: "80%"
  },
  navBar_title: {
    marginLeft: 3,
    paddingTop: 4,
    fontWeight: "800",
    fontSize: 19,
    color: "white"
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 25,
    marginTop: 0
  },
  storeName: {
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
    marginLeft: 13
  },
  userName: {
    color: "white",
    fontSize: 13,
    fontWeight: "300",
    marginLeft: 14
    // fontFamily: "WorkSans-ExtraBold"
  },
  iconSettings: {
    width: 30,
    height: 30,
    marginRight: 18,
    borderRadius: 15
  }
});

export default NavBar;
