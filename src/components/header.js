import * as React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Colors } from "../../utils/const";

const Head = props => {
  return (
    <View style={styles.header}>
      <Text style={{ fontSize: 18, color: "white", fontWeight: "800" }}>
        {props.label}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    justifyContent: "center",
    alignItems: "center",
    height: 55,
    width: "100%",
    backgroundColor: Colors.first
  }
});

export default Head;
