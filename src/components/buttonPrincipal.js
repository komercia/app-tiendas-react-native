import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { Colors } from "../../utils/const";

const ButtonPrincipal = ({ link, label, color }) => (
  <TouchableOpacity style={styles.container}>
    <Text style={styles.buttonText}>{label}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    height: 48,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    backgroundColor: Colors.first,
    elevation: 1
  },
  buttonText: {
    color: "white",
    fontSize: 14
  }
});

export default ButtonPrincipal;
