import React from "react";
import { LoginNavigator, DashNavigator } from "./Router";
import { connect } from "react-redux";
import {
  createSwitchNavigator,
  createAppContainer,
  TabNavigator
} from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import AuthLoadingScreen from "../navigation/AuthLoadingScreen";

const AppStack = createStackNavigator({ DashNavigator });
const AuthStack = createStackNavigator({ LoginNavigator });

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack
    },
    {
      initialRouteName: "AuthLoading"
    }
  )
);

// class Auth extends React.Component {
//   render() {
//     if (this.props.auth.auth) {
//       return <DashNavigator />;
//     } else {
//       return <LoginNavigator />;
//     }
//   }
// }

// const mapStateToProps = state => {
//   return {
//     auth: state.auth
//   };
// };

// export default connect(mapStateToProps)(Auth);
