import React from "react";
import { createDrawerNavigator } from "react-navigation";
import Dashboard from "../screens/Dashboard"; //Tab Nav
import ListProducts from "../screens/products/ListProducts";

export default createDrawerNavigator({
  Dashboard: {
    screen: Dashboard,
    navigationOptions: {
      drawerLabel: "Dashboard"
    }
  },
  ListProducts: {
    screen: ListProducts,
    navigationOptions: {
      drawerLabel: "ListProducts"
    }
  }
});
