import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  ActivityIndicator,
  TouchableOpacity, SafeAreaView
} from "react-native";
import {
  Container,
  Content,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right
} from "native-base";
import Icon from "react-native-vector-icons/Feather";
import API from "../../utils/api";
import ButtonPrincipal from "../../src/components/buttonPrincipal";
import TabBar from "../../src/components/tabBar";
import { TextStyle } from "../../utils/textStyles";
import Head from "../../src/components/header";
import { Colors } from "../../utils/const";

export default class ListCostumers extends Component {
  constructor() {
    super();
    this.state = {
      costumers: "",
      loading: true
    };
  }

  async componentDidMount() {
    const costumers = await API.getCostumers();

    this.setState({
      costumers: costumers.clientes,
      loading: false
    });
  }

  listCostumers() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#f14b5a" />
        </View>
      );
    }
    if (this.state.costumers.length == 0) {
      return (
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            height: 350
          }}
        >
          <Image
            style={{ width: 240, height: 240, marginBottom: 15 }}
            resizeMode="contain"
            source={require("../../src/assets/illustration/illustration.png")}
          />
          <Text>No tienes clientes registrados.</Text>
          {/* <ButtonPrincipal
            label="Registrar Cliente"
            link="NewCostumer"
            navigation={this.props.navigation}
          /> */}
        </View>
      );
    }
    return (
      <FlatList
        style={styles.flatList}
        data={this.state.costumers}
        keyExtractor={(item, _) => item.nombre}
        renderItem={({ item }) => (
          <ListItem
            onPress={() => navigate("DetailsCostumer", { costumer: item })}
            thumbnail
            style={{
              backgroundColor: "white",
              paddingHorizontal: 7,
              marginBottom: 6,
              width: "90%"
            }}
          >
            <Left>
              <Thumbnail
                style={{ width: 40, height: 40 }}
                circle
                source={{
                  uri: `https://api2.komercia.co/users/${item.foto}`
                }}
              />
            </Left>
            <Body>
              <Text style={styles.name}>{item.nombre}</Text>
              <Text note numberOfLines={1}>
                {item.email}
              </Text>
            </Body>
            <Right>
              <Icon name="arrow-right" style={styles.icon} />
            </Right>
          </ListItem>
        )}
      />
    );
    rr;
  }

  createCostumer = () => {
    const { navigate } = this.props.navigation;
    return (
      <TouchableOpacity
        style={styles.contentCreate}
        onPress={() => navigate("NewCostumer")}
      >
        <View style={styles.cicrleCreate}>
          <Icon name="plus" style={{ color: "white", fontSize: 18 }} />
        </View>
        <Text style={TextStyle.textLink}>Crear Cliente</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex:1}}>
      <Container style={{ backgroundColor: "#f5f7fa" }}>
        <Head label="Clientes" />
        {/* <View style={{ alignItems: "center" }}>{this.createCostumer()}</View> */}
        <Content>{this.listCostumers()}</Content>
        <TabBar navigation={this.props.navigation} position={4} />
      </Container>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  flatList: {
    marginTop: 10
  },
  icon: {
    color: "blue",
    fontSize: 20
  },
  name: {
    color: "black",
    fontSize: 12,
    fontWeight: "600"
  },
  iconHeader: {
    color: "white",
    fontSize: 18,
    marginLeft: 6
  },
  contentCreate: {
    flexDirection: "row",
    width: "90%",
    height: 50,
    marginTop: 10,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  cicrleCreate: {
    backgroundColor: Colors.second,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 38,
    width: 38,
    marginRight: 10,
    elevation: 2
  }
});
