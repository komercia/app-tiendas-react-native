import React, { Component } from "react";
import { BackHandler, Platform, WebView } from "react-native";

export default class Blog extends Component {
  webView = {
    canGoBack: false,
    ref: null
  };

  onAndroidBackPress = () => {
    if (this.webView.canGoBack && this.webView.ref) {
      this.webView.ref.goBack();
      return true;
    }
    return false;
  };

  componentWillMount() {
    if (Platform.OS === "android") {
      BackHandler.addEventListener(
        "hardwareBackPress",
        this.onAndroidBackPress
      );
    }
  }

  componentWillUnmount() {
    if (Platform.OS === "android") {
      BackHandler.removeEventListener("hardwareBackPress");
    }
  }

  render() {
    return (
      <WebView
        source={{ uri: "https://medium.com/komercia/tagged/ecommerce" }}
        style={{ marginTop: -135 }}
        ref={webView => {
          this.webView.ref = webView;
        }}
        onNavigationStateChange={navState => {
          this.webView.canGoBack = navState.canGoBack;
        }}
      />
    );
  }
}
