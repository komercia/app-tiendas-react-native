import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  SafeAreaView,
  ScrollView
} from "react-native";
import {
  Container,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Tab,
  Tabs,
  TabHeading
} from "native-base";
import Icon from "react-native-vector-icons/Feather";
import API from "../../utils/api";
import { SalesState, Colors, formatCurrency } from "../../utils/const";
import TabBar from "../../src/components/tabBar";
import { TextStyle } from "../../utils/textStyles";
import Head from "../../src/components/header";
import moment from "moment";
import "moment/locale/es";

export default class ListCostumers extends Component {
  constructor() {
    super();
    this.state = {
      currentDate: new Date(),
      markedDate: moment(new Date()).format("YYYY-MM-DD"),
      salesData: "",
      loading: true
    };
  }

  async componentDidMount() {
    const salesData = await API.getSales();
    this.setState({
      salesData: salesData,
      loading: false
    });
  }

  cotizaciones() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#f14b5a" />
        </View>
      );
    }
    if (this.state.salesData.cotizaciones.length == 0) {
      return (
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            height: 400
          }}
        >
          <Image
            style={{ width: 230, height: 230, marginBottom: 15 }}
            resizeMode="contain"
            source={require("../../src/assets/illustration/illustration.png")}
          />
          <Text>Aún no tienes cotizaciones registradas.</Text>
        </View>
      );
    }
    return (
      <FlatList
        style={styles.flatList}
        data={this.state.salesData.cotizaciones}
        keyExtractor={(item, _) => item.fecha}
        renderItem={({ item }) => (
          <TouchableHighlight
            onPress={() =>
              navigate("DetailsProducts", {
                product: item
              })
            }
          >
            <List>
              <ListItem thumbnail>
                <Left style={{ width: 80 }}>
                  <Text style={styles.name}>{item.id}</Text>
                </Left>
                <Body
                  style={{
                    flexDirection: "row",
                    justifyContent: "flex-start",
                    alignItems: "center"
                  }}
                >
                  <Text style={{ width: 110 }}>{item.fecha}</Text>
                  {SalesState(item.estado)}
                </Body>

                <Right />
              </ListItem>
            </List>
          </TouchableHighlight>
        )}
      />
    );
  }

  ventas() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <Container
          style={{
            height: 320,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <ActivityIndicator size="large" color={Colors.second} />
        </Container>
      );
    }
    if (this.state.salesData.ventas.length == 0) {
      return (
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            height: 400
          }}
        >
          <Image
            style={{ width: 210, height: 210, marginBottom: 15 }}
            resizeMode="contain"
            source={require("../../src/assets/illustration/illustration2.png")}
          />
          <Text>Aún no tienes ventas registradas.</Text>
        </View>
      );
    }
    return (
      <FlatList
        data={this.state.salesData.ventas}
        keyExtractor={(item, _) => item.id}
        showsHorizontalScrollIndicator={true}
        removeClippedSubviews
        initialNumToRender={10}
        style={{ flex: 1, width: "100%" }}
        windowSize={15}
        removeClippedSubviews={true}
        pagingEnabled
        directionalLockEnabled
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() =>
              navigate("SaleDetails", {
                sale: item
              })
            }
          >
            <View style={styles.listItem}>
              <View style={{ width: 42 }}>
                <Text style={{ fontSize: 13, color: "gray" }}>{item.id}</Text>
              </View>

              <View
                style={{
                  width: 95,
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                {SalesState(item.estado)}
              </View>

              <View
                style={{ alignItems: "flex-start", width: 112, marginLeft: 6 }}
              >
                <Text
                  style={{ textAlign: "right", textTransform: "capitalize" }}
                >
                  {moment(item.fecha).calendar(null, {
                    lastDay: "[Ayer]",
                    sameDay: "[Hoy]",
                    nextDay: "[Mañana]",
                    lastWeek: "dddd",
                    nextWeek: "dddd",
                    sameElse: "L"
                  })}
                </Text>
                <Text style={{ textAlign: "right", color: "gray" }}>
                  {/* {moment(item.created_at).calendar()} */}
                  {moment(item.created_at)
                    .startOf("hour")
                    .fromNow()}
                </Text>
              </View>

              <View style={{ flex: 3, alignItems: "flex-end" }}>
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 15,
                    color: "black"
                  }}
                >
                  $ {formatCurrency(item.total)}
                </Text>
              </View>
            </View>

            <View style={styles.separator} />
          </TouchableOpacity>
        )}
      />
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Head label="Mis ventas" />
        <Container>
          <Tabs tabBarUnderlineStyle={{ backgroundColor: Colors.first }}>
            <Tab
              heading={
                <TabHeading style={{ backgroundColor: "#f5f7fa" }}>
                  <Text style={{ color: "black" }}>Ventas</Text>
                </TabHeading>
              }
            >
              {/* <ScrollView>
                <View style={{ flex: 1 }}>{this.ventas()}</View>
              </ScrollView> */}
              <ScrollView>
                <Content>{this.ventas()}</Content>
              </ScrollView>
            </Tab>
            <Tab
              heading={
                <TabHeading style={{ backgroundColor: "#f5f7fa" }}>
                  <Text style={{ color: "black" }}>Cotizaciones</Text>
                </TabHeading>
              }
            >
              <ScrollView>{this.cotizaciones()}</ScrollView>
            </Tab>
          </Tabs>
        </Container>
        {/* 
        <View style={styles.contentTotal}>
          <Text style={styles.textTotal}>Total Ventas</Text>
          <Text style={styles.priceTotal}>$ 2.450.243</Text>
        </View> */}
        {/* <View
          style={{ width: "100%", height: 2, backgroundColor: "#1abc9c" }}
        /> */}
        <TabBar navigation={this.props.navigation} position={3} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    color: "blue",
    fontSize: 20
  },
  name: {
    color: "black",
    fontSize: 16
  },
  title: {
    color: "black",
    fontSize: 18,
    fontWeight: "bold"
  },
  container: {
    flex: 1,
    height: 500,
    alignItems: "center",
    alignContent: "center"
  },
  flatList: {
    overflow: "scroll"
  },
  contentTotal: {
    flexDirection: "row",
    height: 30,
    paddingHorizontal: 20,
    height: 40,
    backgroundColor: "#303456",
    justifyContent: "space-around",
    alignItems: "center",
    borderColor: "#E5E5E5",
    borderTopWidth: 0.5,
    paddingHorizontal: 5,
    elevation: 3
  },
  listItem: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    overflow: "scroll",
    marginBottom: 10,
    marginTop: 15,
    paddingHorizontal: 14
  },
  priceTotal: {
    fontSize: 22,
    color: "white"
  },
  textTotal: {
    color: "white"
  },
  iconHeader: {
    color: "white",
    fontSize: 18,
    marginLeft: 6
  },
  separator: {
    backgroundColor: "#ecf0f1",
    height: 1,
    width: "100%",
    marginVertical: 5
  },
  contentCreate: {
    flexDirection: "row",
    width: "90%",
    height: 50,
    marginTop: 10,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  cicrleCreate: {
    backgroundColor: "#ff585a",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 38,
    width: 38,
    marginRight: 10,
    elevation: 2
  }
});
