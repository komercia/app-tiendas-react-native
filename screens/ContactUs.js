import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
  ImageBackground,
  ScrollView,
  SafeAreaView
} from "react-native";
import { Container, Button } from "native-base";
import FastImage from "react-native-fast-image";
import { Colors } from "../utils/const";
import Icon from "react-native-vector-icons/Feather";

export default class ContactUs extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      messageError: false
    };
  }

  header() {
    const { goBack } = this.props.navigation;
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.header}>
        <Button transparent onPress={() => goBack()}>
          <Icon name="arrow-left" color="black" size={30} />
        </Button>
      </View>
    );
  }

  getHelp() {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{
          width: "100%",
          borderRadius: 5,
          height: 110,
          backgroundColor: "#ADEDDF",
          alignSelf: "center",
          flexDirection: "row",
          overflow: "hidden",
          marginTop: 0,
          marginBottom: 25
        }}
      >
        <View
          style={{
            flex: 3,
            padding: 16,
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text style={{ color: Colors.first }}>
            Visita nuestro centro de ayuda online, donde aprenderás todo sobre
            la plataforma.{" "}
          </Text>
          <TouchableOpacity
            onPress={() => navigate("Register")}
            style={styles.buttonLogin}
          >
            {/* <View style={styles.alignButton}>
              <Text style={styles.buttonText}>Escríbenos</Text>
            </View> */}
          </TouchableOpacity>
        </View>
        <ImageBackground
          style={{ width: "100%", height: "100%", flex: 3 }}
          source={require("../src/assets/smilegirl.jpg")}
        />
      </View>
    );
  }

  getFacebookGroup() {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{
          width: "100%",
          borderRadius: 5,
          height: 110,
          backgroundColor: "#007dd3",
          alignSelf: "center",
          flexDirection: "row",
          overflow: "hidden",
          marginTop: 0,
          marginBottom: 25
        }}
      >
        <View
          style={{
            flex: 3,
            padding: 16,
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text style={{ color: "white" }}>
            Unete a nuestra comunidad de comercio electronico en Facebook.{" "}
          </Text>
          <TouchableOpacity
            onPress={() => navigate("Register")}
            style={styles.buttonLogin}
          >
            {/* <View style={styles.alignButton}>
              <Text style={styles.buttonText}>Escríbenos</Text>
            </View> */}
          </TouchableOpacity>
        </View>
        <ImageBackground
          style={{ width: "100%", height: "100%", flex: 3 }}
          source={{
            uri:
              "https://images.unsplash.com/photo-1517292987719-0369a794ec0f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=967&q=80"
          }}
        />
      </View>
    );
  }

  getWhastaspp() {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{
          width: "100%",
          borderRadius: 5,
          height: 110,
          backgroundColor: "#455A64",
          alignSelf: "center",
          flexDirection: "row",
          overflow: "hidden",
          marginTop: 0,
          marginBottom: 25
        }}
      >
        <View
          style={{
            flex: 3,
            padding: 16,
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text style={{ color: "white" }}>
            Escribe a nuestro whatsapp, en caso que requieras soporte inmediato.{" "}
          </Text>
          <TouchableOpacity
            onPress={() => navigate("Register")}
            style={styles.buttonLogin}
          >
            {/* <View style={styles.alignButton}>
              <Text style={styles.buttonText}>Escríbenos</Text>
            </View> */}
          </TouchableOpacity>
        </View>
        <ImageBackground
          style={{ width: "100%", height: "100%", flex: 3 }}
          source={{
            uri:
              "https://images.pexels.com/photos/46924/pexels-photo-46924.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
          }}
        />
      </View>
    );
  }

  _getButtonWhatsapp() {
    return (
      <TouchableOpacity
        style={styles.buttonWhatsapp}
        onPress={() => {
          Linking.openURL("http://api.whatsapp.com/send?phone=57" + 3123760628);
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          {/* <Image
            style={{ width: 16, height: 16 }}
            source={{
              uri:
                "https://www.stickpng.com/assets/images/5a4e2ef62da5ad73df7efe6e.png"
            }}
          /> */}
          <Text
            style={{
              color: "white",
              textAlign: "center",
              paddingLeft: 10,
              paddingRight: 10,
              fontSize: 16,
              fontWeight: "bold"
            }}
          >
            WhatsApp
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  _getButtonFacebook() {
    return (
      <TouchableOpacity
        style={styles.buttonFacebook}
        onPress={() => {
          Linking.openURL("https://www.facebook.com/groups/komercia.co/");
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          {/* <Image
            style={{ width: 16, height: 16 }}
            source={{
              uri:
                "https://www.stickpng.com/assets/images/5a4e2ef62da5ad73df7efe6e.png"
            }}
          /> */}
          <Text
            style={{
              color: "white",
              textAlign: "center",
              paddingLeft: 10,
              paddingRight: 10,
              fontSize: 16,
              fontWeight: "900"
            }}
          >
            Comunidad Facebook
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  _getButtonHelpDesk() {
    return (
      <TouchableOpacity
        style={styles.buttonHelpDesk}
        onPress={() => {
          Linking.openURL("https://ayuda.komercia.co");
        }}
      >
        <View style={{ alignItems: "center" }}>
          <Text
            style={{
              color: "white",
              textAlign: "center",
              paddingLeft: 10,
              paddingRight: 10,
              fontSize: 16,
              fontWeight: "900"
            }}
          >
            Centro de Ayuda
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Container style={styles.container}>
          {this.header()}
          <ScrollView style={styles.content}>
            <Text style={styles.title}>Comunidad Facebook</Text>
            {this.getFacebookGroup()}
            {this._getButtonFacebook()}

            <Text style={styles.title}>Chat con nostros</Text>
            {this.getWhastaspp()}
            {this._getButtonWhatsapp()}

            <Text style={styles.title}>Centro de Ayuda</Text>
            {this.getHelp()}
            {this._getButtonHelpDesk()}
          </ScrollView>
        </Container>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "transparent",
    alignItems: "center",
    flexDirection: "column",
    paddingBottom: 10,
    backgroundColor: "white"
  },
  header: {
    backgroundColor: "white",
    flexDirection: "row",
    paddingHorizontal: 15,
    height: 55,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between"
    // zIndex: 999
  },
  content: {
    flex: 1,
    width: "100%",
    padding: 20,
    paddingTop: 5
  },
  buttonWhatsapp: {
    width: "100%",
    backgroundColor: "#25D366",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 23,
    marginTop: -18
  },
  buttonFacebook: {
    width: "100%",
    backgroundColor: "#3b5998",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 23,
    marginTop: -18
  },
  buttonHelpDesk: {
    width: "100%",
    backgroundColor: Colors.first,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 23,
    marginTop: -18
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  },
  title: {
    fontSize: 18,
    color: "black",
    fontWeight: "900",
    marginBottom: 6
  }
});
