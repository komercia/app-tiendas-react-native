import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
  Linking,
  SafeAreaView
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import NavBar from "../src/components/navBar";
import TabBar from "../src/components/tabBar";
import API from "../utils/api";
import LottieView from "lottie-react-native";
import FastImage from "react-native-fast-image";
import { Colors } from "../utils/const";
import Modal from "react-native-modal";
import { connect } from "react-redux";

class DashBoard extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      userData: "",
      storeData: "",
      inventory: "",
      totalInventory: "",
      sales: "",
      isModalVisible: false,
      versionAndroid: "1.1.9",
      versionIOS: "1.1.3",
      modalUserNotStore: false
    };
  }

  async componentDidMount() {
    const userData = await API.getUser();
    const storeData = await API.getStore();
    const products = await API.getInventory();
    const sales = await API.getSales();
    const versionAndroid = await API.getVersionAppAndroid();
    const versionIOS = await API.getVersionAppIOS();
    if (Platform.OS === "ios") {
      if (this.state.versionIOS != versionIOS) {
        this.setState({
          isModalVisible: true
        });
      }
    }
    if (Platform.OS === "android") {
      if (this.state.versionAndroid != versionAndroid) {
        this.setState({
          isModalVisible: true
        });
      }
    }
    this.setState({
      userData: userData,
      storeData: storeData,
      totalInventory: products,
      inventory: products.slice(0, 5),
      sales: sales.ventas,
      loading: false
    });

    if (userData.tienda == 0) {
      this.setState({
        modalUserNotStore: true
      });
    }
  }

  formatCurrency = n => {
    if (n) {
      return parseInt(n)
        .toFixed()
        .replace(/(\d)(?=(\d{3})+(,|$))/g, "$1,");
    } else {
      return 0;
    }
  };

  salesValue = () => {
    let suma = (items, prop) => {
      return items.reduce((a, b) => {
        return a + b[prop];
      }, 0);
    };

    let total = suma(this.state.sales, "total");
    return total;
  };

  getStoreView() {
    let url = "www";
    let link = "";
    // Si la tienda tiene dominio
    if (this.state.storeData.informacion_tienda[0].dominio) {
      url = this.shortURL(this.state.storeData.informacion_tienda[0].dominio);
      link = this.state.storeData.informacion_tienda[0].dominio;
    }
    // Si la tienda no tiene dominio
    else {
      url = `${this.state.storeData.subdominio}.komercia.co`;
      link = `http://${this.state.storeData.subdominio}.komercia.co`;
    }
    return (
      <View
        style={{
          backgroundColor: "#FFFFFF",
          width: 280,
          alignItems: "center",
          alignSelf: "center",
          padding: 5,
          borderRadius: 5,
          margin: 20,
          top: -85,
          marginBottom: -70,
          elevation: 1
        }}
      >
        <FastImage
          style={styles.photoUser}
          resizeMode="contain"
          source={{
            uri: `https://api2.komercia.co/logos/${this.state.storeData.logo}`
          }}
        />
        <Text style={styles.nameUser}>{this.state.storeData.nombre}</Text>
        <Text style={styles.urlText}>{url}</Text>

        <View
          style={{
            backgroundColor: "#F8F8F8",
            width: 250,
            margin: 10,
            padding: 6,
            borderRadius: 5
          }}
        >
          <View style={styles.containerStoreData}>
            <View style={styles.viewBackCard}>
              <Text style={styles.textBackCard}>Ventas</Text>
              <Text style={styles.secondTextBackCard}>
                {this.state.sales.length}
              </Text>
            </View>

            <View style={styles.viewBackCard}>
              <Text style={styles.textBackCard}>Visitas</Text>
              {this.state.storeData.cantidad_visitas.length ? (
                <Text style={styles.secondTextBackCard}>
                  {this.state.storeData.cantidad_visitas[0].numero_visitas}
                </Text>
              ) : (
                <Text style={styles.secondTextBackCard}>0</Text>
              )}
            </View>
          </View>
          <View style={styles.containerStoreData}>
            <View style={styles.viewBackCard}>
              <Text style={styles.textBackCard}>Productos </Text>
              <Text style={styles.secondTextBackCard}>
                {this.state.totalInventory.length}
              </Text>
            </View>
            <View style={styles.viewBackCard}>
              <Text style={styles.textBackCard}>Ingresos</Text>
              <Text style={styles.secondTextBackCard}>
                $ {this.formatCurrency(this.salesValue())}
              </Text>
            </View>
          </View>
        </View>
        {/* <TouchableOpacity
          onPress={() => {
            Linking.openURL(link);
          }}
          style={styles.buttonShowStore}
        >
          <View style={styles.alignButton}>
            <Text style={styles.buttonText}>Tienda online</Text>
          </View>
        </TouchableOpacity> */}
      </View>
    );
  }

  getHorizontalFeed() {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground
        // source={require("../src/assets/background.png")}
        style={styles.layoutFeed}
      >
        <TouchableOpacity onPress={() => navigate("DetailsProducts")}>
          <ImageBackground
            style={styles.feedContent}
            source={require("../src/assets/iconhearth.png")}
          >
            <Text style={styles.textFeedContent}>Subir Producto</Text>
          </ImageBackground>
        </TouchableOpacity>
        <FlatList
          showsHorizontalScrollIndicator={true}
          horizontal={true}
          data={this.state.feed}
          keyExtractor={(item, _) => item.image}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                navigate("DetailsProducts", {
                  product: item
                })
              }
            >
              <ImageBackground
                style={styles.feedContent}
                source={{
                  uri: item.image
                }}
              >
                <Text style={styles.textFeedContent}>{item.text}</Text>
              </ImageBackground>
            </TouchableOpacity>
          )}
        />
      </ImageBackground>
    );
  }

  getCoupleButtons() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.coupleButtons}>
        <View style={styles.menu_buttons2}>
          <TouchableOpacity
            style={styles.menuItem3}
            onPress={() => navigate("NewSaleGeneralDetails")}
          >
            <FastImage
              style={styles.iconButton2}
              source={require("../src/assets/send2.png")}
            />
            <Text style={styles.menuTitle2}>Vender</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItem3}
            onPress={() => navigate("Settings")}
          >
            <FastImage
              style={styles.iconButton2}
              source={require("../src/assets/group.png")}
            />
            <Text style={styles.menuTitle2}>Tienda</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  getMenuButtons() {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground
        // source={require("../src/assets/background.png")}
        style={styles.layoutFeed}
      >
        <View style={styles.menu_buttons2}>
          <TouchableOpacity
            style={styles.menuItem2}
            onPress={() => navigate("Settings")}
          >
            <FastImage
              style={styles.iconButton}
              source={require("../src/assets/iconbag.png")}
            />
            <Text style={styles.menuTitle}>Tienda</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.menuItem2}
            onPress={() => navigate("ListCostumers")}
          >
            <FastImage
              style={styles.iconButton}
              source={require("../src/assets/iconhearth.png")}
            />
            <Text style={styles.menuTitle}>Clientes</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.menu_buttons2}>
          <TouchableOpacity
            style={styles.menuItem2}
            onPress={() => navigate("ContactUs")}
          >
            <FastImage
              style={styles.iconButton}
              source={require("../src/assets/iconorange.png")}
            />
            <Text style={styles.menuTitle}>Ayuda</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.menuItem2}
            onPress={() => navigate("Sales")}
          >
            <FastImage
              style={styles.iconButton}
              source={require("../src/assets/iconpurple.png")}
            />
            <Text style={styles.menuTitle}>Ventas</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }

  getTopCard() {
    return (
      <View style={styles.imageBack}>
        <ImageBackground
          style={{
            width: "97%",
            height: 110,
            borderRadius: 5,
            backgroundColor: "white",
            marginTop: -60,
            elevation: 1
          }}
          imageStyle={{ borderRadius: 8 }}
          // source={require("../src/assets/clipped.png")}
        >
          <View
            style={{
              padding: 21,
              height: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              alignContent: "center"
            }}
          >
            <View style={styles.viewBackCard}>
              <Text style={styles.textBackCard}>Productos </Text>
              <Text style={styles.secondTextBackCard}>
                {this.state.totalInventory.length}
              </Text>
            </View>

            <View style={styles.viewBackCard}>
              <Text style={styles.textBackCard}>
                {" "}
                {/* <Icon name="award" style={styles.tabIcon} />  */}
                Ventas{" "}
              </Text>
              <Text style={styles.secondTextBackCard}>
                {this.state.sales.length}
              </Text>
            </View>
            <View style={styles.viewBackCard}>
              <Text style={styles.textBackCard}>
                {" "}
                {/* <Icon name="heart" style={styles.tabIcon} />  */}
                Ingresos{" "}
              </Text>
              <Text style={styles.secondTextBackCard}>
                $ {this.formatCurrency(this.salesValue())}
              </Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }

  getHorizontalInventory() {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground
        // source={require("../src/assets/background.png")}
        style={styles.layoutHorizontalFeed}
      >
        <TouchableOpacity
          style={{
            flexDirection: "column",
            width: 75,
            height: 120,
            padding: 10,
            paddingVertical: 18,
            alignItems: "center",
            borderRadius: 3,
            justifyContent: "space-between",
            backgroundColor: "#DFE7F5",
            marginRight: 8
          }}
          onPress={() => navigate("NewProductInfo")}
        >
          <FastImage
            style={{ width: 32, height: 32 }}
            source={require("../src/assets/plus2.png")}
          />
          <Text
            style={{
              color: "#0047CC",
              alignSelf: "center",
              textAlign: "center",
              fontWeight: "bold",
              letterSpacing: 0.1,
              fontSize: 11
            }}
          >
            Crear producto
          </Text>
        </TouchableOpacity>

        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.state.inventory}
          keyExtractor={(item, _) => item.nombre}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={{
                flexDirection: "column",
                width: 90,
                height: 120,
                padding: 1,
                paddingVertical: 8,
                alignItems: "center",
                borderRadius: 3,
                justifyContent: "space-between",
                backgroundColor: "white",
                marginRight: 9
              }}
              onPress={() =>
                navigate("DetailsProducts", {
                  product: item
                })
              }
            >
              <FastImage
                style={{ width: 73, height: 73, borderRadius: 3 }}
                source={{ uri: this.fitImage(item.foto_cloudinary) }}
              />
              <Text
                style={{
                  color: "#2f3542",
                  alignSelf: "center",
                  textAlign: "center",
                  fontWeight: "600",
                  letterSpacing: 0.04,
                  fontSize: 12,
                  width: 50,
                  overflow: "hidden",
                  height: 20
                }}
              >
                {item.nombre.split(" ")[0]}
              </Text>
            </TouchableOpacity>
          )}
        />
      </ImageBackground>
    );
  }

  getHelp() {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{
          width: "87%",
          borderRadius: 5,
          height: 170,
          backgroundColor: "#ADEDDF",
          alignSelf: "center",
          flexDirection: "row",
          flex: 1,
          overflow: "hidden",
          marginTop: -15,
          marginBottom: 30
        }}
      >
        <View style={{ flex: 3, padding: 16, justifyContent: "space-between" }}>
          <Text style={{ color: Colors.first }}>
            Vamos a crecer juntos,{" "}
            <Text style={{ fontWeight: "bold" }}>
              escríbenos tus ideas y opiniones,
            </Text>{" "}
            queremos ayudarte a crecer.
          </Text>
          <TouchableOpacity
            onPress={() => navigate("ContactUs")}
            style={styles.buttonLogin}
          >
            <View style={styles.alignButton}>
              <Text style={styles.buttonText}>Escríbenos</Text>
            </View>
          </TouchableOpacity>
        </View>
        <ImageBackground
          style={{ width: "100%", height: "100%", flex: 2 }}
          // source={{
          //   uri:
          //     "https://cdn1.ticbeat.com/src/uploads/2018/02/trabajadores-tecnologicos-programadores-810x540.jpg?x32709"
          // }}
          source={require("../src/assets/smilegirl.jpg")}
        />
      </View>
    );
  }

  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      "https://res.cloudinary.com/komercia-store/image/upload/w_160,q_auto,f_auto/" +
      fitImage[1]
    );
  }

  shortURL(url) {
    let newUrl = url.split("//");
    return newUrl[1];
  }

  // getSalesChannel() {
  //   let url = "";
  //   let link = "";
  //   // Si la tienda tiene dominio
  //   if (this.state.storeData.informacion_tienda[0].dominio) {
  //     url = this.shortURL(this.state.storeData.informacion_tienda[0].dominio);
  //     link = this.state.storeData.informacion_tienda[0].dominio;
  //   }
  //   // Si la tienda no tiene dominio
  //   else {
  //     url = `${this.state.storeData.subdominio}.komercia.co`;
  //     link = `http://${this.state.storeData.subdominio}.komercia.co`;
  //   }

  //   return (
  //     <View
  //       style={{
  //         width: "87%",
  //         backgroundColor: "white",
  //         alignSelf: "center",
  //         flexDirection: "row",
  //         paddingHorizontal: 0,
  //         height: 55,
  //         borderRadius: 5,
  //         elevation: 1,
  //         padding: 4,
  //         alignItems: "center",
  //         justifyContent: "space-around",
  //         marginBottom: 10
  //       }}
  //     >
  //       <Text style={{ color: "black", fontWeight: "700" }}>{url}</Text>

  //       <View style={styles.viewBackCard}>
  //         <Text style={styles.textBackCard}>Productos </Text>
  //         <Text style={styles.secondTextBackCard}>
  //           {this.state.totalInventory.length}
  //         </Text>
  //       </View>

  //       <View style={styles.viewBackCard}>
  //         <Text style={styles.textBackCard}>
  //           {" "}
  //           {/* <Icon name="award" style={styles.tabIcon} />  */}
  //           Ventas{" "}
  //         </Text>
  //         <Text style={styles.secondTextBackCard}>
  //           {this.state.sales.length}
  //         </Text>
  //       </View>
  //       <View style={styles.viewBackCard}>
  //         <Text style={styles.textBackCard}>
  //           {" "}
  //           {/* <Icon name="heart" style={styles.tabIcon} />  */}
  //           Ingresos{" "}
  //         </Text>
  //         <Text style={styles.secondTextBackCard}>
  //           $ {this.formatCurrency(this.salesValue())}
  //         </Text>
  //       </View>
  //       {/* <Image style={{width: 50, height: 50}} source={{uri:"https://img.icons8.com/clouds/2x/store-front.png"}}></Image> */}
  //       <TouchableOpacity
  //         onPress={() => {
  //           Linking.openURL(link);
  //         }}
  //         style={styles.buttonShowStore}
  //       >
  //         <View style={styles.alignButton}>
  //           <Text style={styles.buttonText}>Ver tienda</Text>
  //         </View>
  //       </TouchableOpacity>
  //     </View>
  //   );
  // }

  modal() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.modalContent}>
        <ImageBackground
          style={{ width: "100%", height: "100%", flex: 4 }}
          source={{
            uri:
              "https://images.pexels.com/photos/1484661/pexels-photo-1484661.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
          }}
        />
        <View
          style={{
            width: "100%",
            height: "100%",
            flex: 3,
            padding: 16,
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: Colors.first,
              fontSize: 16,
              alignItems: "center",
              padding: 5,
              paddingHorizontal: 10
            }}
          >
            Actualiza tu aplicación,{" "}
            <Text style={{ fontWeight: "bold" }}>
              hemos lanzado nuevas funcionalidades{" "}
            </Text>
            en la nueva versión.
          </Text>

          <View style={{ width: "100%", alignItems: "center" }}>
            {Platform.OS === "android" ? (
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL(
                    "https://play.google.com/store/apps/details?id=com.kontrol"
                  );
                }}
                style={styles.buttonLogin}
              >
                <View style={styles.alignButton}>
                  <Text style={styles.buttonText}>Actualizar</Text>
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL(
                    "https://apps.apple.com/us/app/komercia-pos/id1477983869"
                  );
                }}
                style={styles.buttonLogin}
              >
                <View style={styles.alignButton}>
                  <Text style={styles.buttonText}>Actualizar</Text>
                </View>
              </TouchableOpacity>
            )}

            <TouchableOpacity
              onPress={() => {
                this.setState({ isModalVisible: false });
              }}
              style={styles.finishButton}
            >
              <Text style={{ color: "gray", marginTop: 15 }}>
                Actualizar más tarde
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  logout() {
    const { navigate } = this.props.navigation;
    this.props.dispatch({
      type: "LOGOUT"
    });
    navigate("Login");
  }

  modalNoStore() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.modalContent}>
        <ImageBackground
          style={{ width: "100%", height: "100%", flex: 4 }}
          source={{
            uri:
              "https://images.pexels.com/photos/288477/pexels-photo-288477.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
          }}
        />
        <View
          style={{
            width: "100%",
            height: "100%",
            flex: 3,
            padding: 16,
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: Colors.first,
              fontSize: 16,
              alignItems: "center",
              padding: 5,
              paddingHorizontal: 10
            }}
          >
            Lo sentimos,{" "}
            <Text style={{ fontWeight: "bold" }}>
              no apareces vinculado a ninguna tienda o tienes una cuenta de
              comprador.{" "}
            </Text>
            Si eres administrador de una tienda envianos un correo para revisar
            tu caso gustavo@komercia.co
          </Text>

          <View style={{ width: "100%", alignItems: "center" }}>
            <TouchableOpacity
              onPress={() => this.logout()}
              style={styles.buttonLogin}
            >
              <View style={styles.alignButton}>
                <Text style={styles.buttonText}>Salir</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.containerLoading}>
          <LottieView
            style={{
              width: 120,
              backgroundColor: "white",
              alignSelf: "center"
            }}
            autoPlay
            loop
            source={require("../src/animations/bag.json")}
          />
          <Image
            style={{
              width: 140,
              height: 35,
              alignSelf: "center",
              marginTop: 150
            }}
            source={require("../src/logos/logo3.png")}
          />
        </View>
      );
    }
    return (
      <SafeAreaView style={styles.container}>
        <Modal isVisible={this.state.isModalVisible}>{this.modal()}</Modal>
        <Modal isVisible={this.state.modalUserNotStore}>
          {this.modalNoStore()}
        </Modal>
        <ScrollView style={styles.body}>
          <ImageBackground
            resizeMode="cover"
            style={{
              width: "100%",
              backgroundColor: "rgba(68, 41, 180, 1.5)"
            }}
            // source={require("../src/assets/frame.png")}
            // source={require("../src/assets/loginImage2.png")}
          >
            <NavBar
              user={this.state.userData}
              store={this.state.storeData}
              navigation={this.props.navigation}
            />
          </ImageBackground>
          <View style={styles.menu_container}>
            {this.getStoreView()}
            {/* {this.getTopCard()} */}
            {/* {this.getCoupleButtons()} */}
            {/* <View style={styles.layoutText}>
              <Text style={styles.textTitle}>Herramientas</Text>
            </View> */}

            {this.getMenuButtons()}

            {/* <View style={styles.layoutText}>
              <Text style={styles.textTitle}>Formas de vender</Text>
            </View> */}

            {/* {this.getSalesChannel()} */}

            <View style={styles.layoutText}>
              <Text style={styles.textTitle}>Inventario</Text>
            </View>
            {this.getHorizontalInventory()}
            {this.getHelp()}
          </View>
        </ScrollView>
        <TabBar navigation={this.props.navigation} position={1} />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#f5f7fa",
    justifyContent: "center",
    alignContent: "center"
  },
  containerLoading: {
    flex: 1,
    width: "100%",
    flexDirection: "column",
    backgroundColor: "white",
    justifyContent: "center",
    alignContent: "center"
  },
  body: {
    flex: 1
    // backgroundColor: Colors.first
  },
  layoutText: {
    paddingHorizontal: 26,
    marginBottom: 8,
    marginTop: 12
  },
  textTitle: {
    fontSize: 18,
    letterSpacing: 0.13,
    color: "#042C5C",
    fontWeight: "900"
  },
  menu_container: {
    backgroundColor: "#E9ECF5",
    // backgroundColor: "#f5f7fa",
    paddingTop: 0
  },
  coupleButtons: {
    flex: 1,
    alignSelf: "center",
    width: "100%",
    justifyContent: "space-between",
    paddingHorizontal: 10
  },
  menu_buttons: {
    marginTop: 6,
    marginBottom: 8,
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: 10
  },
  menuItem: {
    width: "45%",
    height: 80,
    borderRadius: 5,
    elevation: 2,
    alignItems: "center",
    justifyContent: "center"
  },
  menu_button_bottom: {
    paddingTop: 6,
    paddingBottom: 6,
    backgroundColor: "#DCDEE5",
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: 10,
    elevation: 1,
    borderColor: "#E5E5E5",
    borderTopWidth: 0.5
  },
  menuNewSale: {
    backgroundColor: "#3F46AD",
    width: "95%",
    height: 45,
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    elevation: 1
  },
  menuIcon: {
    color: "#313456",
    fontSize: 30
  },
  menuTitle: {
    color: "#313456",
    marginTop: 0,
    fontSize: 13
  },
  containerStoreData: {
    padding: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignContent: "center"
  },
  menuTitle2: {
    color: "#0047CC",
    marginTop: 0,
    fontSize: 13,
    fontWeight: "bold"
  },
  menuIconSale: {
    color: "white",
    fontSize: 18,
    marginRight: 10
  },
  menuTitleSale: {
    color: "white",
    fontSize: 16
  },
  flatList: {
    flex: 1
  },
  feedContent: {
    width: 200,
    height: 80,
    borderRadius: 5,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10
  },
  textFeedContent: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    textShadowColor: "rgba(0,0,0, .75)",
    textShadowOffset: {
      width: 2,
      height: 2
    },
    textShadowRadius: 0
  },
  InventoryContent: {
    width: 110,
    height: 100,
    borderRadius: 5,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
    backgroundColor: "white"
  },
  textFeedContent: {
    color: "white",
    fontSize: 18,
    fontWeight: "bold",
    textShadowColor: "rgba(0,0,0, .75)",
    textShadowOffset: {
      width: 2,
      height: 2
    },
    textShadowRadius: 0
  },
  layoutFeed: {
    paddingVertical: 3,
    paddingHorizontal: 8,
    marginBottom: 8
  },
  layoutHorizontalFeed: {
    flexDirection: "row",
    paddingVertical: 8,
    paddingHorizontal: 27,
    marginBottom: 35
  },
  titleLayout: {
    color: "#4c4c4c",
    fontSize: 14,
    marginBottom: 10,
    fontWeight: "600"
  },
  toolsTextContent: {
    paddingHorizontal: 17,
    marginTop: 12,
    width: "100%"
  },
  imageBack: {
    width: "100%",
    height: 70,
    borderRadius: 10,
    marginTop: 0,
    marginBottom: 0,
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: 18
  },
  detailsBackCard: {
    padding: 10
  },
  textBackCard: {
    color: Colors.first,
    fontSize: 12
  },
  viewBackCard: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  secondTextBackCard: {
    color: "black",
    fontSize: 15,
    fontWeight: "bold"
  },
  button1: {
    color: "#00E4E5"
  },
  tabIcon: {
    color: "#fff",
    fontSize: 12,
    marginBottom: 8,
    fontWeight: "100"
  },
  button2: {
    color: "#E1CE66"
  },
  menuItem2: {
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    alignItems: "center",
    alignContent: "center",
    width: "48%",
    borderRadius: 5,
    padding: 3,
    paddingVertical: 8,
    elevation: 1
  },
  menuItem3: {
    backgroundColor: "#DFE7F5",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    alignItems: "center",
    alignContent: "center",
    width: "48%",
    borderRadius: 5,
    padding: 3,
    paddingVertical: 12
  },
  menu_buttons2: {
    marginTop: 5,
    marginBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    width: "100%"
  },
  iconButton: {
    width: 30,
    height: 30,
    marginRight: 10,
    marginLeft: 10
  },
  iconButton2: {
    width: 16,
    height: 16,
    marginRight: 10,
    marginLeft: 15,
    marginRight: 16
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 14,
    letterSpacing: 0.13,
    fontWeight: "800"
  },
  buttonLogin: {
    width: "72%",
    backgroundColor: Colors.first,
    height: 38,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  buttonShowStore: {
    width: 250,
    backgroundColor: Colors.first,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginTop: 0
  },
  modalContent: {
    flexDirection: "column",
    borderRadius: 8,
    height: 390,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    overflow: "hidden",
    backgroundColor: "#ADEDDF"
  },
  photoUser: {
    width: 150,
    height: 50,
    marginVertical: 10,
    backgroundColor: "white",
    borderRadius: 6
  },
  nameUser: {
    width: 180,
    fontWeight: "700",
    fontSize: 16,
    textAlign: "center",
    color: "black"
  }
});

const mapStateToProps = state => {
  return { auth: state };
};

export default connect(mapStateToProps)(DashBoard);
