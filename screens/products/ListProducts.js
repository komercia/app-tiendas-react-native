import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  Platform,
  ScrollView,
  SafeAreaView
} from "react-native";
import {
  Container,
  Header,
  Body,
  Title,
  Right,
  Left,
  Button,
  Badge,
  Fab
} from "native-base";
import API from "../../utils/api";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/Feather";
import FastImage from "react-native-fast-image";
import { formatCurrency, Colors } from "../../utils/const";
import TabBar from "../../src/components/tabBar";
import { TextStyle } from "../../utils/textStyles";
import Head from "../../src/components/header";

class ListProducts extends React.Component {
  constructor() {
    super();
    this.state = {
      inventory: [],
      loading: true,
      selected: "Key1",
      active: false
    };
  }
  async componentDidMount() {
    const products = await API.getInventory();
    this.setState({
      inventory: products,
      loading: false
    });
  }
  onValueChange(value) {
    this.setState({
      selected: value
    });
  }
  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      "https://res.cloudinary.com/komercia-store/image/upload/w_160,q_auto,f_auto/" +
      fitImage[1]
    );
  }
  _getProducts() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#30c490" />
        </View>
      );
    }
    if (this.state.inventory.length == 0) {
      return (
        <View
          style={{
            alignItems: "center",
            alignContent: "center",
            justifyContent: "center",
            height: 450
          }}
        >
          <FastImage
            style={{ width: 110, height: 110, marginBottom: 20 }}
            source={require("../../src/assets/empty-box.png")}
          />
          <Text>No tienes productos registrados.</Text>
          <TouchableOpacity
            onPress={() => navigate("NewProductInfo")}
            style={{
              marginTop: 20,
              padding: 10,
              backgroundColor: Colors.first
            }}
          >
            <Text style={{ color: "white" }}>Agrega tu primer producto</Text>
          </TouchableOpacity>
        </View>
      );
    }
    return (
      <FlatList
        style={styles.flatList}
        // initialScrollIndex={20}
        // windowSize={10}
        removeClippedSubviews={true}
        // maxToRenderPerBatch={10}
        // updateCellsBatchingPeriod={20}
        data={this.state.inventory}
        keyExtractor={(item, _) => item.nombre + item.id}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() =>
              navigate("DetailsProducts", {
                product: item
              })
            }
          >
            <View style={styles.itemList}>
              <View style={styles.infoProduct}>
                <View
                  style={{
                    padding: 8,
                    backgroundColor: "white",
                    borderRadius: 7
                  }}
                >
                  <FastImage
                    style={styles.imageProduct}
                    resizeMode={FastImage.resizeMode.cover}
                    source={{ uri: this.fitImage(item.foto_cloudinary) }}
                  />
                </View>

                <View style={styles.itemListText}>
                  <Text style={styles.nameProduct}>{item.nombre}</Text>

                  {item.precio > 0 && item.con_variante == 0 && (
                    <Text style={styles.priceProduct}>
                      $ {formatCurrency(item.precio)}
                    </Text>
                  )}

                  {item.precio > 0 && item.con_variante == null && (
                    <Text style={styles.priceProduct}>
                      $ {formatCurrency(item.precio)}
                    </Text>
                  )}

                  {item.precio == 0 && item.con_variante == 0 && (
                    <View
                      style={{
                        padding: 5,
                        marginVertical: 3,
                        alignItems: "center",
                        justifyContent: "center",
                        width: 130,
                        borderRadius: 5,
                        backgroundColor: "#B0C4DE"
                      }}
                    >
                      <Text style={{ color: "#2f3542", fontSize: 12 }}>
                        Producto sin precio
                      </Text>
                    </View>
                  )}

                  {item.precio == 0 && item.con_variante == 1 && (
                    <View
                      style={{
                        padding: 5,
                        marginVertical: 3,
                        alignItems: "center",
                        justifyContent: "center",
                        width: 130,
                        borderRadius: 5,
                        backgroundColor: "#B0C4DE"
                      }}
                    >
                      <Text style={{ color: "#2f3542", fontSize: 12 }}>
                        Precio según variante
                      </Text>
                    </View>
                  )}

                  {item.inventario > 0 && item.con_variante == 0 && (
                    <Text>{item.inventario} unidades </Text>
                  )}

                  {item.inventario > 0 && item.con_variante == null && (
                    <Text>{item.inventario} unidades </Text>
                  )}

                  {item.inventario == 0 && item.con_variante == 0 && (
                    <View
                      style={{
                        padding: 5,
                        marginVertical: 3,
                        alignItems: "center",
                        justifyContent: "center",
                        width: 130,
                        borderRadius: 5,
                        backgroundColor: "#f14b5a"
                      }}
                    >
                      <Text style={{ color: "white", fontSize: 12 }}>
                        Inventario Agotado!
                      </Text>
                    </View>
                  )}

                  {item.inventario == 0 && item.con_variante == 1 && (
                    <View
                      style={{
                        padding: 5,
                        marginVertical: 3,
                        alignItems: "center",
                        justifyContent: "center",
                        width: 130,
                        borderRadius: 5,
                        backgroundColor: "#30c490"
                      }}
                    >
                      <Text style={{ color: "white", fontSize: 12 }}>
                        Diferentes referencias
                      </Text>
                    </View>
                  )}

                  {/* {item.visitas_producto.numero_visitas ? (
                    <Text style={{ color: "#27ae60" }}>
                      <Icon name="eye" /> {item.visitas_producto.numero_visitas}{" "}
                      vistas
                    </Text>
                  ) : (
                    <Text style={{ color: "#27ae60" }}>
                      <Icon name="eye" /> 0 vistas
                    </Text>
                  )} */}
                </View>
                {/* <View style={styles.itemListTextLeft}>
                  <Text style={{ color: "#6E6BFD" }}>
                    <Icon name="pocket" /> {item.visitas} ventas
                  </Text>
                </View> */}
                {/* <TouchableOpacity
                  onPress={() => {
                    this.props.addItemToCart(item);
                  }}
                  style={styles.buttonAddCart}
                >
                  <View style={styles.alignButton}>
                    <Text
                      style={{
                        fontSize: 25,
                        color: Colors.first,
                        fontWeight: "400"
                      }}
                    >
                      {" "}
                      +{" "}
                    </Text>
                  </View>
                </TouchableOpacity> */}
              </View>
              <View style={styles.separator} />
            </View>
          </TouchableOpacity>
        )}
      />
    );
  }
  header() {
    const { navigate } = this.props.navigation;
    const { goBack } = this.props.navigation;
    return (
      <Header>
        <Left>
          <Button transparent onPress={() => goBack()}>
            <Icon name="arrow-left" color="white" size={20} />
          </Button>
        </Left>
        <Body>
          <Title>Inventario</Title>
        </Body>
        <Right>
          <TouchableOpacity
            style={styles.cancelButton}
            onPress={() => navigate("NewProductInfo")}
          >
            <Text style={{ color: "white", fontSize: 13 }}>Nuevo producto</Text>
            <Icon name="plus-circle" style={styles.icon} />
          </TouchableOpacity>
        </Right>
      </Header>
    );
  }

  newProduct = () => {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          width: "95%"
        }}
      >
        <TouchableOpacity
          style={styles.contentCreate}
          onPress={() => navigate("NewProductInfo")}
        >
          <View style={styles.cicrleCreate}>
            <Icon name="plus" style={{ color: "white", fontSize: 18 }} />
          </View>
          <Text style={TextStyle.textLink}>Nuevo Producto</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.cancelButton}
          onPress={() => navigate("NewSaleBarcode")}
        >
          <Text style={{ color: "black", fontSize: 13 }}>Escaner </Text>
          <FastImage
            style={{
              width: 36,
              height: 25,
              alignSelf: "center"
            }}
            source={require("../../src/assets/barcode2.png")}
          />
        </TouchableOpacity>
      </View>
    );
  };

  buttonNewProduct = () => {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ flexDirection: "row" }}>
        <TouchableOpacity
          style={{
            flexDirection: "row",
            width: "46%",
            height: 48,
            margin: 8,
            padding: 10,
            paddingVertical: 12,
            borderRadius: 5,
            justifyContent: "flex-start",
            alignItems: "center",
            backgroundColor: "#DFE7F5",
            marginRight: 4,
            alignSelf: "flex-start"
          }}
          onPress={() => navigate("NewProductInfo")}
        >
          <FastImage
            style={{ width: 32, height: 32, marginRight: 10 }}
            source={require("../../src/assets/plus2.png")}
          />
          <Text
            style={{
              color: "#0047CC",
              fontWeight: "bold",
              letterSpacing: 0.1,
              fontSize: 13
            }}
          >
            Crear producto
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity
          style={{
            flexDirection: "row",
            width: "46%",
            height: 48,
            margin: 8,
            padding: 10,
            paddingVertical: 12,
            borderRadius: 5,
            justifyContent: "flex-start",
            alignItems: "center",
            backgroundColor: "Colors.second",
            marginRight: 8,
            alignSelf: "flex-start"
          }}
          onPress={() => navigate("NewProductInfo")}
        >
          <FastImage
            style={{ width: 32, height: 32, marginRight: 10 }}
            source={require("../../src/assets/plus2.png")}
          />
          <Text
            style={{
              color: "#0047CC",
              fontWeight: "bold",
              letterSpacing: 0.1,
              fontSize: 13
            }}
          >
            Código de barras
          </Text>
        </TouchableOpacity> */}
      </View>
    );
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Container style={{ backgroundColor: "#f7f7f7" }}>
          {Platform.OS === "android" && (
            <Fab
              active={this.state.active}
              // onPress={() => this.setState({ active: !this.state.active })}
              onPress={() => navigate("NewProductInfo")}
              direction="down"
              position="topRight"
              style={{
                backgroundColor: "#30c490",
                zIndex: 999,
                width: 50,
                height: 50
              }}
            >
              <Icon name="plus" />
              <Button style={{ backgroundColor: "#34A34F" }}>
                <Text>hola</Text>
                <Icon name="logo-whatsapp" />
              </Button>
              <Button style={{ backgroundColor: "#3B5998" }}>
                <Icon name="logo-facebook" />
              </Button>
              <Button disabled style={{ backgroundColor: "#DD5144" }}>
                <Icon name="mail" />
              </Button>
            </Fab>
          )}

          <ScrollView>
            <Head label="Inventario" />

            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                width: "100%"
              }}
            >
              {/* {this.newProduct()} */}
            </View>
            {Platform.OS === "ios" && <View>{this.buttonNewProduct()}</View>}

            <View style={styles.body}>{this._getProducts()}</View>
          </ScrollView>
          <TabBar navigation={this.props.navigation} position={2} />
        </Container>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    cartItems: state.cartItems
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: id => dispatch({ type: "ADD_TO_CART", payload: id })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListProducts);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: 400,
    backgroundColor: "#f7f7f7",
    alignItems: "center",
    justifyContent: "space-around"
  },
  body: {
    flex: 1,
    backgroundColor: "#f7f7f7"
  },
  topText: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 40,
    marginHorizontal: 10,
    paddingHorizontal: 15
  },
  length: {
    fontSize: 16,
    color: "#4c4c4c"
  },
  picker: {
    width: 145,
    alignItems: "flex-end"
  },
  flatList: {
    width: "100%",
    marginTop: 10,
    flex: 1
  },
  itemList: {
    width: "95%",
    flexDirection: "column",
    marginHorizontal: 10
  },
  infoProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    paddingRight: 10
  },
  itemListText: {
    flex: 5,
    marginLeft: 10,
    justifyContent: "flex-start"
  },
  itemListTextLeft: {
    marginLeft: 10,
    justifyContent: "flex-end"
  },
  nameProduct: {
    fontWeight: "400",
    color: "#7f8c8d",
    fontSize: 14
  },
  imageProduct: {
    // flex: 2,
    width: 70,
    height: 70,
    paddingVertical: 10,
    overflow: "hidden",
    borderRadius: 5,
    //resizeMode: "contain",
    backgroundColor: "white"
  },
  priceProduct: {
    fontWeight: "bold",
    color: "#2c3e50",
    fontSize: 18
  },
  separator: {
    backgroundColor: "#ecf0f1",
    height: 1,
    width: "100%",
    marginVertical: 10
  },
  statsProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15,
    marginBottom: 5
  },
  buttonProduct: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  buttonDetails: {
    fontWeight: "bold",
    color: "#008c9d"
  },
  buttonEdit: {
    fontWeight: "bold",
    color: "#0f9380"
  },
  icon: {
    color: "white",
    fontSize: 18,
    marginLeft: 6
  },
  iconView: {
    color: "#34495e",
    fontSize: 16
  },
  buttonAddCart: {
    backgroundColor: "white",
    padding: 5,
    paddingVertical: 1,
    marginVertical: 1,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: Colors.first
  },
  cancelButton: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  contentCreate: {
    flexDirection: "row",
    width: "40%",
    height: 50,
    marginTop: 9,
    marginBottom: 9,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  cicrleCreate: {
    backgroundColor: Colors.second,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 38,
    width: 38,
    marginRight: 10,
    elevation: 2
  }
});
