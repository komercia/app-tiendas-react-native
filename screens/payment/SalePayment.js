import React from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import { Container, Title, Text, Button } from "native-base";
import Modal from "react-native-modal";

export default class SalePayment extends React.Component {
  constructor() {
    super();
    this.state = {
      isModalVisible: false,
      showModal: true
    };
  }
  _toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
    setTimeout(() => this.setState({ showModal: false }), 3000);
  };
  modal() {
    const { navigate } = this.props.navigation;
    if (this.state.showModal) {
      return (
        <View style={styles.modalContent}>
          <View
            style={{
              paddingTop: 5,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ActivityIndicator size="large" color="#f14b5a" />
            <Text>Guardando información</Text>
            <Text>Espere un momento por favor</Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.modalContent}>
          <Image
            style={{ width: 40, height: 40, marginBottom: 10 }}
            source={require("../../src/assets/check-mark.png")}
          />
          <Text style={styles.modalTitle}>Venta registrada! </Text>
          <Text style={styles.modalText}>
            Se ha enviado una correo al cliente con los detalles de la compra{" "}
          </Text>

          <TouchableOpacity
            onPress={() => {
              this.setState({ isModalVisible: false }), navigate("Sales");
            }}
            style={styles.finishButton}
          >
            <Text style={{ color: "white" }}>Continuar</Text>
          </TouchableOpacity>
        </View>
      );
    }
  }
  render() {
    const { goBack } = this.props.navigation;
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Modal isVisible={this.state.isModalVisible}>{this.modal()}</Modal>
        <ScrollView>
          <View style={styles.header}>
            <Button transparent onPress={() => goBack()}>
              <Icon name="arrow-left" color="black" size={30} />
            </Button>
          </View>
          <View style={styles.paymentsDetails}>
            <Text style={styles.title}>Medios de pago</Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                flexWrap: "wrap",
                marginTop: 15
              }}
            >
              <TouchableOpacity style={styles.buttonPayment}>
                <Image source={require("../../src/assets/datafono.jpg")} />
                <Text style={styles.textButton}>Datafono</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.buttonPayment}>
                <Image source={require("../../src/assets/cheque.jpg")} />
                <Text style={styles.textButton}>Cheque</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.buttonPayment}>
                <Image source={require("../../src/assets/cash.jpg")} />
                <Text style={styles.textButton}>Efectivo</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.buttonPayment}>
                <Image source={require("../../src/assets/credito.jpg")} />
                <Text style={styles.textButton}>Tarjeta Crédito</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.saleDetails}>
            <View>
              <Text>Total a pagar</Text>
              <Text>
                COP{" "}
                <Image
                  source={require("../../src/assets/colombia-icon.png")}
                  style={{ width: 16, height: 16, marginLeft: 10 }}
                />
              </Text>
            </View>
            <Title style={{ color: "black", fontSize: 30 }}>$ 350.000</Title>
          </View>
        </ScrollView>

        <View style={styles.footer}>
          <Button
            onPress={this._toggleModal}
            block
            style={{
              margin: 5,
              elevation: 4,
              marginHorizontal: 18,
              backgroundColor: "#4834d4"
            }}
          >
            <Text style={{ color: "white", fontSize: 20 }}>Guardar</Text>
          </Button>
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: "white",
    height: 40,
    marginTop: 10,
    paddingHorizontal: 20,
    alignItems: "flex-start",
    justifyContent: "center"
  },

  socialIcon: {
    padding: 30
  },
  title: {
    fontSize: 20,
    color: "black",
    fontWeight: "600",
    marginBottom: 10
  },
  saleDetails: {
    flex: 1,
    backgroundColor: "#F1F1F2",
    width: "100%",
    padding: 20,
    marginBottom: 15,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  paymentsDetails: {
    flex: 1,
    backgroundColor: "#FFF",
    width: "100%",
    padding: 20,
    marginBottom: 15,
    justifyContent: "space-between"
  },
  buttonPayment: {
    elevation: 2,
    padding: 8,
    width: "48%",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 2,
    marginBottom: 20
  },
  textButton: {
    marginTop: 8,
    color: "black"
  },
  modalContent: {
    flexDirection: "column",
    backgroundColor: "white",
    borderRadius: 8,
    height: 240,
    justifyContent: "center",
    alignItems: "center"
  },
  modalIcon: {
    color: "gray",
    fontSize: 35,
    marginBottom: 10,
    marginTop: 10
  },
  modalTitle: {
    fontSize: 18,
    fontWeight: "600",
    marginBottom: 15,
    alignSelf: "center"
  },
  modalText: {
    textAlign: "center",
    paddingHorizontal: 30,
    fontSize: 14
  },
  finishButton: {
    backgroundColor: "#3F46AD",
    borderRadius: 2,
    padding: 12,
    paddingHorizontal: 90,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    margin: 15,
    width: 250,
    marginTop: 30
  }
});
