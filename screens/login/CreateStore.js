import React from "react";
import API from "../../utils/api";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView
} from "react-native";
import { connect } from "react-redux";
import { Container, Item } from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Colors } from "../../utils/const";

class CreateStore extends React.Component {
  constructor() {
    super();
    this.state = {
      nameStore: null,
      nameUser: null,
      email: null,
      phone: null,
      password: null,
      loading: false,
      messageError: false,
      messageErrorText: ""
    };
  }
  login = () => {
    this.setState({ loading: true });
    API.postLogin(this.state.email, this.state.password)
      .then(response => {
        let token = response.data.access_token;
        console.warn("logueando");
        this.props.dispatch({
          type: "LOGIN",
          payload: {
            token
          }
        });
      })
      .catch(() => {
        console.warn("error en el login");
        this.setState({
          loading: false,
          messageError: true
        });
      });
  };
  onPressRegister = () => {
    this.setState({ loading: true });
    if (this.state.nameStore == null) {
      this.setState({
        loading: false,
        messageError: true,
        messageErrorText: "Debes escribir el nombre de tu empresa"
      });
    } else if (this.state.nameUser == null) {
      this.setState({
        loading: false,
        messageError: true,
        messageErrorText: "Debes escribir tu nombre y apelldo"
      });
    } else if (this.state.phone == null) {
      this.setState({
        loading: false,
        messageError: true,
        messageErrorText: "Debes escribir tu número de telefono"
      });
    } else if (this.state.email == null) {
      this.setState({
        loading: false,
        messageError: true,
        messageErrorText: "Debes escribir un correo electronico"
      });
    } else if (this.state.password == null) {
      this.setState({
        loading: false,
        messageError: true,
        messageErrorText: "Debes escribir una constraseña"
      });
    } else {
      this.setState({
        messageError: false,
        messageErrorText: ""
      });
      API.createStore(
        this.state.nameStore,
        this.state.nameUser,
        this.state.email,
        this.state.phone,
        this.state.password
      )
        .then(response => {
          console.warn("registro exitoso");
          this.login();
        })
        .catch(response => {
          console.warn(response);
          this.setState({
            loading: false,
            messageError: true,
            messageErrorText:
              "Tienes algún error en el formulario o el correo ya se encuentra registrado"
          });
        });
    }
  };
  _getButtonRegister() {
    if (this.state.loading) {
      return (
        <TouchableOpacity style={styles.buttonLogin}>
          <ActivityIndicator size="large" color="#ffff" />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={this.onPressRegister}
          style={styles.buttonLogin}
        >
          <View style={styles.alignButton}>
            <Text style={styles.buttonText}>Crear cuenta</Text>
          </View>
        </TouchableOpacity>
      );
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAwareScrollView
        style={{ backgroundColor: "transparent", flex: 1, height: "100%" }}
      >
        <Container style={styles.container}>
          <Image
            style={{
              width: 60,
              height: 60,
              position: "absolute",
              left: -20,
              bottom: -20
            }}
            resizeMode="contain"
            source={require("../../src/assets/circle.png")}
          />
          <Image
            style={{
              width: 90,
              height: 90,
              position: "absolute",
              right: -40,
              bottom: -30
            }}
            resizeMode="contain"
            source={require("../../src/assets/rectangulo.png")}
          />
          <Image
            style={{
              width: 70,
              height: 70,
              position: "absolute",
              left: -10,
              top: -25
            }}
            resizeMode="contain"
            source={require("../../src/assets/yellowForma.png")}
          />
          <Image
            style={{
              width: 15,
              height: 15,
              position: "absolute",
              right: 50,
              top: 65
            }}
            resizeMode="contain"
            source={require("../../src/assets/x.png")}
          />
          <Image
            style={{
              flex: 2,
              width: 130,
              height: 80,
              marginBottom: 3,
              marginTop: 20,
              alignSelf: "center"
            }}
            resizeMode="contain"
            source={require("../../src/logos/logo1.png")}
          />

          <View style={styles.header}>
            <Text style={styles.title}>Registrar Empresa </Text>
          </View>

          {this.state.messageError == true && (
            <View style={styles.errorLogin}>
              <Text style={styles.textError}>
                {this.state.messageErrorText}
              </Text>
            </View>
          )}

          <TextInput
            ref={input => {
              this.input1 = input;
            }}
            onSubmitEditing={() => {
              this.input1.blur();
              this.input2.focus();
            }}
            returnKeyType="next"
            blurOnSubmit={false}
            placeholderTextColor="#59617b"
            placeholder={"Nombre de tu empresa"}
            style={styles.input}
            underlineColorAndroid="rgba(0,0,0,0)"
            onChangeText={nameStore => this.setState({ nameStore })}
          />

          <TextInput
            ref={input => {
              this.input1 = input;
            }}
            onSubmitEditing={() => {
              this.input1.blur();
              this.input2.focus();
            }}
            returnKeyType="next"
            blurOnSubmit={false}
            placeholderTextColor="#59617b"
            placeholder={"Tu nombre y apellido"}
            style={styles.input}
            underlineColorAndroid="rgba(0,0,0,0)"
            onChangeText={nameUser => this.setState({ nameUser })}
          />

          <TextInput
            ref={input => {
              this.input1 = input;
            }}
            onSubmitEditing={() => {
              this.input1.blur();
              this.input2.focus();
            }}
            returnKeyType="next"
            blurOnSubmit={false}
            placeholderTextColor="#59617b"
            placeholder={"Tu Teléfono "}
            style={styles.input}
            underlineColorAndroid="rgba(0,0,0,0)"
            onChangeText={phone => this.setState({ phone })}
          />

          <TextInput
            ref={input => {
              this.input1 = input;
            }}
            onSubmitEditing={() => {
              this.input1.blur();
              this.input2.focus();
            }}
            returnKeyType="next"
            blurOnSubmit={false}
            placeholderTextColor="#59617b"
            placeholder={"Correo Electrónico"}
            style={styles.input}
            underlineColorAndroid="rgba(0,0,0,0)"
            onChangeText={email => this.setState({ email })}
          />

          <TextInput
            ref={input => {
              this.input2 = input;
            }}
            blurOnSubmit={false}
            placeholderTextColor="#59617b"
            placeholder={"Tu Contraseña"}
            style={styles.input}
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            onChangeText={password => this.setState({ password })}
          />
          {this._getButtonRegister()}

          <View style={styles.forgetPass}>
            <Text style={styles.link} onPress={() => navigate("LoginMail")}>
              Ya estoy registrado,{" "}
              <Text
                style={{
                  fontWeight: "700",
                  color: Colors.first
                }}
              >
                Iniciar sesión
              </Text>
            </Text>
          </View>

          <View style={styles.footer}>
            <Text style={styles.textFooter}>
              Al hacer click aceptas
              <Text style={styles.linkFooter} onPress={() => navigate("Terms")}>
                {" "}
                los terminos y condiciones |{" "}
              </Text>
              <Text style={styles.linkFooter} onPress={() => navigate("Terms")}>
                políticas de privacidad
              </Text>{" "}
              de Komercia.co
            </Text>
          </View>
        </Container>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "column",
    backgroundColor: "#ffffff"
  },
  header: {
    flex: 1,
    width: 290,
    alignContent: "flex-start",
    justifyContent: "flex-start",
    backgroundColor: "#ffffff",
    marginVertical: 10,
    flexDirection: "column"
  },
  textWelcome: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5
  },
  form: {
    flex: 5,
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    fontSize: 16,
    fontWeight: "700",
    textAlign: "left",
    alignSelf: "flex-start",
    color: Colors.first
  },
  subtitle: {
    fontSize: 15,
    fontWeight: "500",
    textAlign: "center",
    marginBottom: 10,
    color: "#59617b"
  },
  link: {
    color: "#59617b",
    alignSelf: "center",
    fontSize: 14,
    fontWeight: "500"
  },
  linkFooter: {
    color: "#59617b",
    alignSelf: "center",
    fontSize: 12,
    fontWeight: "600"
  },
  footer: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    width: 300
  },
  input: {
    backgroundColor: Colors.input_background,
    height: 50,
    borderWidth: 0.5,
    borderColor: "#707070",
    width: 290,
    paddingLeft: 45,
    borderRadius: 5,
    color: "black",
    fontWeight: "600",
    marginBottom: 10
  },
  textFooter: {
    textAlign: "center"
  },
  alignButton: {
    flexDirection: "row",
    alignItems: "center"
  },
  buttonLogin: {
    width: 300,
    // backgroundColor: "#f14b5a",
    backgroundColor: Colors.first,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 5,
    marginTop: 10
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  },
  forgetPass: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 25,
    width: 300
  },
  footer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 15,
    width: 300
  },
  textFooter: {
    color: "#59617b",
    alignSelf: "center",
    textAlign: "center",
    fontSize: 11
  },
  errorLogin: {
    backgroundColor: "white",
    marginBottom: 7,
    width: 300,
    height: 30,
    borderRadius: 5,
    fontWeight: "500",
    justifyContent: "center",
    alignItems: "center"
  },
  textError: {
    textAlign: "center",
    color: "#f14b5a",
    fontSize: 14,
    fontWeight: "700"
  },
  containerButton: {
    height: 48,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    backgroundColor: Colors.first,
    elevation: 1
  }
});

const mapStateToProps = state => {
  return { auth: state };
};

export default connect(mapStateToProps)(CreateStore);
