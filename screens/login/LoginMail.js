import React from "react";
import API from "../../utils/api";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
  SafeAreaView
} from "react-native";
import { connect } from "react-redux";
import { Container, Item } from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Colors, Bold } from "../../utils/const";

class LoginMail extends React.Component {
  constructor() {
    super();
    this.state = {
      email: " ",
      password: " ",
      loading: false,
      messageError: false
    };
  }

  onPressLogin = () => {
    this.setState({ loading: true });
    API.postLogin(this.state.email, this.state.password)
      .then(response => {
        let token = response.data.access_token;
        this.props.dispatch({
          type: "LOGIN",
          payload: {
            token
          }
        });
      })
      .catch(() => {
        this.setState({
          loading: false,
          messageError: true
        });
      });
  };

  _getButtonLogin() {
    if (this.state.loading) {
      return (
        <TouchableOpacity style={styles.buttonLogin}>
          <ActivityIndicator size="large" color="#ffff" />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={this.onPressLogin}
          style={styles.buttonLogin}
        >
          <View style={styles.alignButton}>
            <Text style={styles.buttonText}>Iniciar Sesión</Text>
          </View>
        </TouchableOpacity>
      );
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAwareScrollView>
        <Container>
          <ImageBackground
            style={styles.logoContainer}
            resizeMode="cover"
            source={require("../../src/assets/loginImage2.png")}
          >
            <Image
              style={{
                width: 50,
                height: 50,
                position: "absolute",
                left: 45,
                bottom: 20
              }}
              resizeMode="contain"
              source={require("../../src/assets/circle.png")}
            />
            <Image
              style={{
                width: 100,
                height: 100,
                position: "absolute",
                right: -25,
                bottom: -35
              }}
              resizeMode="contain"
              source={require("../../src/assets/rectangulo.png")}
            />
            <Image
              style={{
                width: 80,
                height: 80,
                position: "absolute",
                left: -10,
                top: -25
              }}
              resizeMode="contain"
              source={require("../../src/assets/yellowForma.png")}
            />
            <Image
              style={{
                width: 15,
                height: 15,
                position: "absolute",
                right: 50,
                top: 65
              }}
              resizeMode="contain"
              source={require("../../src/assets/x.png")}
            />
            <Image
              style={{
                width: 170,
                height: 75,
                marginBottom: 3,
                alignSelf: "center"
              }}
              resizeMode="contain"
              source={require("../../src/logos/logo4.png")}
            />
            <Image
              style={{
                width: 120,
                height: 60,
                alignSelf: "center"
              }}
              resizeMode="contain"
              source={require("../../src/assets/textLogin.png")}
            />
          </ImageBackground>
          <View style={styles.formContainer}>
            {this.state.messageError == true && (
              <View style={styles.errorLogin}>
                <Text style={styles.textError}>
                  Error en los datos, intenta de nuevo. Si no tienes una cuenta,
                  Regístrate!
                </Text>
              </View>
            )}

            <Item>
              <TextInput
                ref={input => {
                  this.input1 = input;
                }}
                onSubmitEditing={() => {
                  this.input1.blur();
                  this.input2.focus();
                }}
                returnKeyType="next"
                blurOnSubmit={false}
                placeholderTextColor="#59617b"
                placeholder={"Correo Electrónico"}
                style={styles.input}
                underlineColorAndroid="rgba(0,0,0,0)"
                onChangeText={email => this.setState({ email })}
              />
            </Item>

            <Item>
              <TextInput
                ref={input => {
                  this.input2 = input;
                }}
                blurOnSubmit={false}
                placeholderTextColor="#59617b"
                placeholder={"Tu Contraseña"}
                style={styles.input}
                secureTextEntry={true}
                underlineColorAndroid="transparent"
                onChangeText={password => this.setState({ password })}
              />
            </Item>

            {this._getButtonLogin()}

            <View style={styles.forgetPass}>
              <Text style={styles.link} onPress={() => navigate("CreateStore")}>
                <Text style={{ fontWeight: "500", fontSize: 14 }}>
                  ¿Eres nuevo?{" "}
                </Text>
                <Text
                  style={{
                    fontWeight: "700",
                    color: Colors.first,
                    fontSize: 14
                  }}
                >
                  Regístrate aquí
                </Text>
              </Text>
            </View>
          </View>

          {/* <View style={styles.footer}>
            <Text style={styles.textFooter}>
              Al hacer click aceptas
              <Text style={styles.link} onPress={() => navigate("Terms")}>
                {" "}
                los terminos y condiciones |{" "}
              </Text>
              <Text style={styles.link} onPress={() => navigate("Terms")}>
                políticas de privacidad
              </Text>{" "}
              de Komercia.co
            </Text>
          </View> */}
        </Container>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "blue"
  },
  logoContainer: {
    flex: 3,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(68, 41, 180, 0.9)",
    position: "relative",
    overflow: "hidden"
  },
  formContainer: {
    flex: 3,
    width: "100%",
    marginTop: 5,
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  link: {
    color: "#59617b",
    alignSelf: "center",
    fontSize: 12,
    fontWeight: "600"
  },
  input: {
    backgroundColor: Colors.input_background,
    height: 50,
    borderWidth: 0.5,
    borderColor: "#707070",
    width: 290,
    paddingLeft: 45,
    borderRadius: 5,
    color: "black",
    fontWeight: "600",
    marginBottom: 10
    // shadowOpacity: 0.3,
    // shadowRadius: 3,
    // shadowColor: "#59617b",
    // shadowColor: "#5c6af6",
    // shadowOffset: { width: 0, height: 2 }
  },
  textFooter: {
    textAlign: "center"
  },
  alignButton: {
    flexDirection: "row",
    alignItems: "center"
  },
  buttonLogin: {
    width: 290,
    backgroundColor: Colors.first,
    height: 48,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 18,
    marginTop: 10
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  },

  forgetPass: {
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    width: 300
  },
  footer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 15,
    width: 300
  },
  textFooter: {
    color: "#59617b",
    alignSelf: "center",
    textAlign: "center",
    fontSize: 12
  },
  errorLogin: {
    backgroundColor: "white",
    marginBottom: 10,
    width: 300,
    height: 40,
    borderRadius: 5,
    fontWeight: "500",
    justifyContent: "center",
    alignItems: "center"
  },
  textError: {
    color: "#B1180F",
    fontSize: 14,
    fontWeight: "500"
  }
});

const mapStateToProps = state => {
  return { auth: state };
};

export default connect(mapStateToProps)(LoginMail);
