import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  Linking
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import { Colors, Bold } from "../../utils/const";
import API from "../../utils/api";
import FastImage from "react-native-fast-image";
import LottieView from "lottie-react-native";

export default class Store extends React.Component {
  constructor() {
    super();
    this.state = { loading: true, storeData: "" };
  }

  async componentDidMount() {
    const storeData = await API.getStore();

    this.setState({
      storeData: storeData,
      loading: false
    });
  }

  shortURL(url) {
    let newUrl = url.split("//");
    return newUrl[1];
  }

  shortInstagram(url) {
    let newUrl = url.split(".com/");
    return newUrl[1];
  }

  getStoreView() {
    let url = "www";
    let link = "";
    // Si la tienda tiene dominio
    if (this.state.storeData.informacion_tienda[0].dominio) {
      url = this.shortURL(this.state.storeData.informacion_tienda[0].dominio);
      link = this.state.storeData.informacion_tienda[0].dominio;
    }
    // Si la tienda no tiene dominio
    else {
      url = `${this.state.storeData.subdominio}.komercia.co`;
      link = `http://${this.state.storeData.subdominio}.komercia.co`;
    }
    return (
      <View
        style={{
          backgroundColor: "#E9ECF5",
          width: "90%",
          alignItems: "center",
          padding: 20,
          borderRadius: 7,
          margin: 25
        }}
      >
        <FastImage
          style={styles.photoUser}
          resizeMode="contain"
          source={{
            uri: `https://api2.komercia.co/logos/${this.state.storeData.logo}`
          }}
        />
        <Text style={styles.nameUser}>{this.state.storeData.nombre}</Text>
        <Text style={styles.urlText}>{url}</Text>

        <TouchableOpacity
          onPress={() => {
            Linking.openURL(link);
          }}
          style={styles.buttonLogin}
        >
          <View style={styles.alignButton}>
            <Text style={styles.buttonText}>Tienda online</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  getItemsView() {
    return (
      <View
        style={{
          backgroundColor: "#E9ECF5",
          width: "100%",
          alignItems: "center",
          padding: 20
        }}
      >
        <View style={{ alignSelf: "flex-start", marginBottom: 15 }}>
          <Text style={styles.textTitle}>Información tienda</Text>
        </View>
        <View
          style={{
            backgroundColor: "#FFFFFF",
            width: "100%",
            alignItems: "flex-start",
            padding: 12,
            paddingTop: 20,
            borderRadius: 5
          }}
        >
          <View style={styles.itemSetting}>
            <Icon name="eye" size={14} color="black" />
            <Text style={styles.textItem}>
              Visitas al sitio:{" "}
              {this.state.storeData.cantidad_visitas.length ? (
                <Text style={styles.secondTextBackCard}>
                  {this.state.storeData.cantidad_visitas[0].numero_visitas}
                </Text>
              ) : (
                <Text style={styles.secondTextBackCard}>0</Text>
              )}
            </Text>
          </View>

          <View style={styles.itemSetting}>
            <Icon name="phone-forwarded" size={14} color="black" />
            <Text style={styles.textItem}>
              Telefono: {this.state.storeData.informacion_tienda[0].telefono}
            </Text>
          </View>
          <View style={styles.itemSetting}>
            <Icon name="mail" size={14} color="black" />
            <Text style={styles.textItem}>
              {this.state.storeData.informacion_tienda[0].email_tienda}
            </Text>
          </View>
        </View>

        <View
          style={{ alignSelf: "flex-start", marginBottom: 15, marginTop: 20 }}
        >
          {/* <Text style={styles.textTitle}>Social media</Text> */}
        </View>
        <View
          style={{
            backgroundColor: "#FFFFFF",
            width: "100%",
            alignItems: "flex-start",
            padding: 12,
            paddingTop: 20,
            borderRadius: 5
          }}
        >
          {/* {this.state.storeData.redes[0].facebook && (
            <View style={styles.itemSetting}>
              <Icon name="facebook" size={14} color="black" />
              <Text style={styles.textItem}>
                {this.state.storeData.redes[0].facebook}
              </Text>
            </View>
          )} */}
          {/* {this.state.storeData.redes[0].whatsapp && (
            <View style={styles.itemSetting}>
              <Icon name="phone" size={14} color="black" />

              <Text style={styles.textItem}>
                Whatsapp: {this.state.storeData.redes[0].whatsapp}
              </Text>
            </View>
          )} */}
          {/* {this.state.storeData.redes[0].instagram && (
            <View style={styles.itemSetting}>
              <Icon name="instagram" size={14} color="black" />
              <Text style={styles.textItem}>
                {this.shortInstagram(this.state.storeData.redes[0].instagram)}
              </Text>
            </View>
          )} */}

          {/* {this.state.storeData.redes[0].twitter && (
            <View style={styles.itemSetting}>
              <Icon name="twitter" size={14} color="black" />
              <Text style={styles.textItem}>
                {this.state.storeData.redes[0].twitter}
              </Text>
            </View>
          )} */}

          {/* {this.state.storeData.redes[0].youtube && (
            <View style={styles.itemSetting}>
              <Icon name="video" size={14} color="black" />
              <Text style={styles.textItem}>
                {this.state.storeData.redes[0].youtube}
              </Text>
            </View>
          )} */}
        </View>
      </View>
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.containerLoading}>
          <LottieView
            style={{
              justifyContent: "center",
              backgroundColor: "white"
            }}
            autoPlay
            loop
            source={require("../../src/animations/bag.json")}
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {this.getStoreView()}
        {this.getItemsView()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center"
  },
  photoUser: {
    width: 160,
    height: 100,
    marginBottom: 20,
    backgroundColor: "white",
    borderRadius: 6
  },
  nameUser: {
    width: 180,
    fontWeight: "700",
    fontSize: 18,
    textAlign: "center",
    color: "black"
  },
  itemList: {
    marginVertical: 20,
    width: "90%",
    alignItems: "flex-start",
    justifyContent: "center"
  },
  itemInfo: {
    flexDirection: "row",
    marginBottom: 7
  },
  itemIcon: {
    marginRight: 8
  },
  itemText: {
    color: "black"
  },
  webSiteLink: {
    width: "100%",
    height: 45,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    elevation: 1,
    marginTop: 5,
    backgroundColor: "#F5F5F5"
  },
  textLink: {
    color: "gray",
    fontSize: 16,
    fontWeight: "400"
  },
  bookingButton: {
    width: "100%",
    height: 45,
    backgroundColor: Colors.red,
    justifyContent: "center",
    alignItems: "center"
  },
  bookingText: {
    color: "white",
    fontWeight: "300",
    fontSize: 16
  },
  buttonLogin: {
    width: 220,
    backgroundColor: Colors.first,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginTop: 20
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  },
  textTitle: {
    fontSize: 18,
    letterSpacing: 0.13,
    color: "#042C5C",
    fontWeight: "900"
  },
  itemSetting: {
    flexDirection: "row",
    marginBottom: 15
  },
  textItem: {
    fontSize: 13,
    fontWeight: "600",
    marginLeft: 10
  }
});
