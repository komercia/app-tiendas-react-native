import { combineReducers } from "redux";
import auth from "./auth";
import cartItems from "./cartItems";

const rootReducer = combineReducers({
  auth,
  cartItems
});

export default rootReducer;
