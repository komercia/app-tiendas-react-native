// import { Z_DEFAULT_STRATEGY } from "zlib";

// let addedItems = [];

// const cartItems = (cart = [], action) => {
//   switch (action.type) {
//     case "ADD_TO_CART":
//       return [...cart, action.payload];
//     case "REMOVE_FROM_CART":
//       return cart.filter(cartItem => cartItem.id !== action.payload.id);
//     case "RESET_CART":
//       return (cart = []);
//   }
//   return cart;
// };

// export default cartItems;

let cart = [];

const cartItems = (state = cart, action) => {
  switch (action.type) {
    case "ADD_TO_CART":
      return [...state, action.payload];
    case "REMOVE_FROM_CART":
      console.warn("delete one");
      return state.filter(cartItem => cartItem.id !== action.payload.id);
    case "RESET_CART":
      console.warn("delete all");
      return (state = []);
  }
  return state;
};

export default cartItems;
